const path = require('path');
const express = require('express');
const createError = require('http-errors');

const indexRouter = require('./index');

module.exports = (app) => {
    // Setup static data
    var publicPath = path.resolve(__dirname + '/../public');
    app.use(express.static(publicPath));

    // Setup routing
    app.use('/', indexRouter);

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        next(createError(404));
    });

    // error handler
    app.use(function (err, req, res, next) {
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};
        res.status(err.status || 500);
        res.render('error');
    });
};
