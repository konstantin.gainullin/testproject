const express = require('express');
const router = express.Router();
const Longpoll = require("express-longpoll");
const Language = require('@google-cloud/language');
const {
  setIntervalAsync,
  clearIntervalAsync 
} = require('set-interval-async/fixed')

var client = new Language.LanguageServiceClient();
var longpoll = Longpoll(router);
var workers = [];


router.get('/', function (req, res, next) {
  return res.render('index', {
    title: 'Nevosoft'
  });
});

router.get('/health', function (req, res, next) {
  return res.redirect('/health/readiness');
});
router.get('/health/:type', function (req, res, next) {
  if (req.params.type == 'readiness') {
    return res.send('OK');
  }
  return res.status(503);
});

router.post('/analyze', function (req, res, next) {
  try {
    let worker = new Worker(req.body.text, req.body.pin)
    workers[worker.id] = worker;
    return res.json(worker.start());
  }
  catch (e) {
    return res.json({ error: e });
  }
});

longpoll.create('/poll/:id', (req, res, next) => {
  req.id = req.params.id;
  next();
});

class Worker {
  constructor(pars, pins) {
    this.id = Worker.nextId;
    this.pins = {}
    this.pars = [];
    Array.from(pins).forEach((v, i) => {
      if (v != 0) {
        this.pins[i] = pars[i];
      } else {
        this.pars.push(pars[i]);
      }
    });
    this.pins = Object.entries(this.pins);
  }

  start() {
    this.workDone = 0;
    this.totalWork = Worker.AllCombinationsCnt(this.pars.length);
    this.permutator = Worker.AllCombinations(this.pars);
    this.texts = [];
    this.confs = [];

    this.timerId = setIntervalAsync(async () => {
      try {
        await this.analyze();
      }
      catch (e) {
        this.error(e);
        this.finish();
      }
    }, 500);

    return {
      url: '/poll/' + this.id
    };
  }

  finish() {
    clearIntervalAsync(this.timerId);
    this.timerId = null;
  }

  composeText(paragraphs) {
    let ps = Array.from(paragraphs);
    this.pins.reverse().forEach(v => {
      ps.splice(v[0], 0, v[1]);
    });
    return ps.join('\r\n');
  }

  async analyze() {
    let perm = this.permutator.next();
    if (perm.done) {
      this.finish();
    } else {
      let text = this.composeText(perm.value);
      let conf = await this.process(text);

      this.texts.push(text);
      this.confs.push(conf);
      this.workDone++;
    }
    this.update(perm);
  }

  async process(text) {
    const document = { content: text, type: 'PLAIN_TEXT' };
    //return [ { name: 'category one', confidence: 0.5 } ];
    const [classification] = await client.classifyText({document});
    //classification.categories.forEach(category => {
    //  `Name: ${category.name}, Confidence: ${category.confidence}`;
    //});
    let m = classification.categories;
    let o = new Map([...m.entries()].sort());
    return o;
  }

  update(perm)
  {
    let data = {
      info: {
        done: perm.done,
        workDone: this.workDone,
        totalWork: this.totalWork,
        progress: (perm.done ? 100 : Math.round(this.workDone * 100 / this.totalWork))
      }
    };

    {
      let bestT, bestC;
      for (let i = 0; i < this.texts.length; ++i) {
        let cs = this.confs[i];
        let c = cs.get(0);
        if (!bestC || bestC.confidence < c.confidence) {
          bestT = this.texts[i];
          bestC = c;
        }
      }
      data.text = bestT;
      data.conf = bestC;
    }

    this.send(data);
  }

  static get nextId() {
    if (Worker._nextId === undefined) {
      Worker._nextId = 1;
    }
    return Worker._nextId++;
  }

  static AllCombinationsCnt(n) {
    return n ? n * Worker.AllCombinationsCnt(n - 1) : 1;
  }

  static * AllCombinations(input) {
    if (input.length == 1) {
        yield input
        return
    }

    for (var i = 0; i < input.length; i++) {
        var tail = input.concat([])
        var head = input[i]
        tail.splice(i, 1)

        for (var combination of Worker.AllCombinations(tail)) {
            yield [head].concat(combination)
        }
    }
  }

  send(data) {
    longpoll.publishToId("/poll/:id", this.id, data);
  }

  error(e) {
    this.send({ error: {
      message: e.message,
      stack: e.stack
    }});
  }
};

module.exports = router;
