FROM node:10.13-alpine
LABEL maintainer konstg.dev@gmail.com
ENV NODE_ENV production
ENV PORT 5000
EXPOSE 5000
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install --production --silent && mv node_modules ../
COPY . .
CMD npm start
